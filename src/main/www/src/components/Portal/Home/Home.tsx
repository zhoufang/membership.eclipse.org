import { Typography } from '@material-ui/core';

export default function Home() {
  return (
    <div
      style={{
        margin: '40px 0',
      }}
    >
      <Typography variant="h4">Home</Typography>
    </div>
  );
}
